#!/bin/bash
echo "=== Begin Singularity ==="
echo "pwd: $(pwd)"
echo "ls:"
ls
source setup.sh
python3 scripts/Converter.py -c Config.cfg --processes 1
echo "=== End Singularity ==="
