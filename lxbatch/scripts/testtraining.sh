#!/bin/bash
echo "=== Begin Singularity ==="
source setup.sh
python3 scripts/Trainer.py -c Config.cfg
python3 scripts/Evaluate.py -c Config.cfg
echo "=== End Singularity ==="

