#!/bin/bash
# Retrieving output path and assigning it
#source output_path.aux
echo "Output Path: ${OUTPUT_PATH}"
#source sing_image.aux
echo "Image used: ${SING_IMAGE}"
echo "Config used: ${CONFIG_FILE}"
mv ${CONFIG_FILE} Config.cfg     # Changing name for easier handling within singularity

mkdir -p /tmp/root_inp/verynewljet/5je_4b_hi_comb2
export EOS_MGM_URL=root://eosatlas.cern.ch
# To be made more dynamic later on (with the help of the config paths ;) )
xrdcp /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/ttHbb_Feb20L2ntuples/verynewljet/5je_4b_hi_comb2/ttH_PowH7_mc16a_AFII.root /tmp/root_inp/verynewljet/5je_4b_hi_comb2/ttH_PowH7_mc16a_AFII.root
xrdcp /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/ttHbb_Feb20L2ntuples/verynewljet/5je_4b_hi_comb2/ttH_PowH7_mc16d_AFII.root /tmp/root_inp/verynewljet/5je_4b_hi_comb2/ttH_PowH7_mc16d_AFII.root
xrdcp /eos/atlas/atlascerngroupdisk/phys-higgs/HSG8/ttHbb_Feb20L2ntuples/verynewljet/5je_4b_hi_comb2/ttH_PowH7_mc16e_AFII.root /tmp/root_inp/verynewljet/5je_4b_hi_comb2/ttH_PowH7_mc16e_AFII.root
echo "Ntuple-import complete!"

singularity exec -H $(pwd) -B /cvmfs -B /afs -B /eos -B /tmp \
  ${SING_IMAGE} \
  bash lxbatch/scripts/testconversion.sh
echo "Conversion complete!"

export EOS_MGM_URL=root://eosuser.cern.ch
xrdcp -rf BatchTestJob_PlotTest ${OUTPUT_PATH}

singularity exec -H $(pwd) -B /cvmfs -B /afs -B /eos -B /tmp \
  ${SING_IMAGE} \
  bash lxbatch/scripts/testtraining.sh

export EOS_MGM_URL=root://eosuser.cern.ch
xrdcp -rf BatchTestJob_PlotTest ${OUTPUT_PATH}

echo "Terminated successfully!"
